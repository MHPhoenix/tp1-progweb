# Chapitre 5

## TP1 Hiver 2019 – Commission Électorale Nationale Indépendante

Dans plusieurs pays francophones, il existe un organe d’appui à la démocratie
appelé Commission Électorale Nationale Indépendante (CENI) ou Commission
Électorale Indépendante (CEI). Comme son nom l’indique, cette institution est
principalement chargée de l’organisation des élections. Les travaux pratiques
de cette session consisteront au développement d’un prototype de site Internet
d’une telle organisation.

## 5.1 Conception du site web

On demande de concevoir et développer un prototype de site Internet pour
une CENI. Idéalement, on aimerait que ce site soit responsive. C’est à dire que
l’affichage s’adapte au média (ordinateur, tablette, mobile) de l’utilisateur. Le
site devra également respecter les spécifications et les exigences reprises ci-après.

### 5.1.1 Spécifications techniques

La réalisation de ce TP nécessitera de développer des pages web en HTML
5, CSS et éventuellement en Javascript. Aucun autre langage ne sera accepté.
A part le CSS bootstrap bootstrap.min.css (**http://getbootstrap.com/**),
aucune librairie autre que la votre ne peut être utilisée. Il convient de préciser
explicitement qu’aucun thème bootstrap n’est autorisé. On peut se servir des
textes, des codes de couleurs et des images disponibles sur les sites suivants :
Textes : **http://lipsum.com/**
Images : **https://pixabay.com/**
Couleur : **https://imagecolorpicker.com/**, **https://htmlcolorcodes.
com/**, **https://www.w3schools.com/colors/**

>Au besoin, on peut utiliser une adresse de courriel factice dans les liens
du site à réaliser.

>Tous les URLs internes au site doivent être des URLs relatifs pour permettre la portabilité du site.

>Le code CSS sera placé dans des fichiers CSS séparés des documents
HTML. Et tous les fichiers CSS seront dans le sous dossier css.

>Un dossier images sera créé pour mettre les images.

La conception et la programmation doit permettre de faire évoluer le site assez
facilement. En effet, le TP 2 sera une extension du TP1.

### 5.1.2 Description du site

Cette section comporte la description des fonctionnalités et des composants
du site. Les aspects visuels et esthétiques sont laissés à votre imagination. Il
convient de garder en esprit que la beauté et l’ergonomie du site font partie de
l’évaluation. Voici quelques fonctionnalités et composants attendus :

>Toutes les pages du site doivent être identifiables par un même design

>Pour ne pas limiter votre innovation, on donne simplement la description
de certaines pages et informations importantes que le site devrait contenir
et on vous laisse organiser le site au mieux possible

>Sauf indication contraire, toutes les pages devraient être accessibles à
partir de la page d’accueil en suivant des liens et/ou des menus.

>La navigation du site doit-être intuitive et l’utilisateur devrait se retrouver facilement sans assistance.

>Choisir des coordonnées (adresses, courriel, téléphone) – factices – de la
CENI.

>Choisir ou créer un logo

>Pour ce prototype, le site portera sur les élections à 3 niveaux : le président de la république, les députés nationaux (ou fédéraux) et les députés
provinciaux.

>Noter que l’élection du président de la république se fait au suffrage
universel direct et il n’y a qu’une seule circonscription qui porte le nom
du pays – Il n’y a donc pas vraiment de choix dans ce cas.

>Pour chaque niveau, préciser (afficher) le nombre des circonscriptions
électorales, les conditions et les documents requis pour déposer sa candidature

>Le dépôt de candidature se fera en ligne et le formulaire devra permettre
de soumettre au moins les éléments suivants :

◇ Niveau de l’élection (un des 3 niveaux indiqués précédemment).
◇ Identités du candidat : nom, prénom, date de naissance et photo. Tous
ces champs sont obligatoires
◇ Parti ou regroupement politique : le candidat devra faire un choix
parmi les noms agréés ou "indépendant" s’il ne fait partie d’aucune
organisation politique agréées. Pour cela, il faut prévoir une liste d’au

### 5.1. CONCEPTION DU SITE WEB

moins 4 parties ou regroupements ainsi que le mot "Indépendant". Le
mot "Indépendant" ne doit pas être au début de la liste mais devra
être choisi par défaut.
◇ Le nom de la circonscription électorale. Ceci sera aussi un choix à
faire selon le niveau électorale. Sauf éventuellement pour l’élection du
président de la république parce qu’il n’y a qu’une seule circonscription.
◇ Les autres informations demandées selon le niveau électoral
◇ Il vous appartient de voir s’il faut un formulaire unique ou plutôt, un
formulaire distinct par niveau électoral.
— La page d’accueil comprendra :
◇ Le logo
◇ Une image (en rapport avec les élections ou le drapeau du pays)
◇ Un slogan
◇ Un menu de navigation
◇ Des options permettant à l’internaute de choisir sa langue entre le
français et l’anglais.
◇ En pratique, le choix de l’anglais conduira sur une page annonçant
que le site est en construction puis redirigera, au bout de 5 secondes,
vers le site en français. De la sorte, le développement du site se fera
réellement dans une seule langue (Français).
— Le menu de navigation contiendra les liens suivants disposés dans l’ordre
qui vous semble approprié :
◇ Contacts affichera le logo et les coordonnées (adresse, téléphone,
courriel) de la CENI. Ainsi que les noms, prénoms, photos, téléphones et courriels du président, du vice-président et du secrétaire
de la CENI.
◇ Enrôlement (Enregistrement des électeurs) donnera à l’utilisateur la
possibilité de remplir les informations suivantes :
○ Prénom (champ firstname), au moins 2 lettres et au maximum
25 lettres (requis)
○ Nom (champ lastname), même exigence que le prénom (requis)
○ Téléphone (champ phone), 10 chiffres (requis)
○ Courriel (champ email), doit être un format de courriel valide
(requis)
○ Adresse de résidence
○ Commentaire (champ Commentaire), zone de texte permettant de
saisir un commentaire
◇ Connexion conduira vers une page de connexion à la partie sécurisée
du site. Cette page aura un formulaire permettant à l’utilisateur de
saisir son nom d’utilisateur (champ username) et son mot de passe
(champ password). Le nom d’utilisateur doit-être formé exactement de 8 caractères –lettres de l’alphabet latin en minuscules et chiffres arabes– et doit commencer par un lettre.
Quant au mot de passe, il doit-être constitué de 10 à 15 caractères alphanumériques (Les 26 lettres de l’alphabet latin – majus-

cules et minuscules acceptés – et les 10 chiffres arabes). Le formulaire
devra donc valider ces exigences avant de soumettre les données.
◇ Carte électorale Affiche la carte du pays avec la capitale et les
différentes provinces. Si on clique sur la capitale, ça mènera vers une
page qui affiche les informations relatives à l’élection présidentielle. Si
on clique sur une province, ça mènera vers une page qui affiche les information relatives aux circonscriptions électorales de cette province.
Pour les provinces, on se limitera à illustrer la faisabilité avec 2 ou 3
provinces seulement.
◇ Publications affiche une page des publications de la CENI
— Le pied de page contiendra les informations suivantes disposées d’une
manière appropriée et cohérente au design global du site
◇ Indication que ce site est développé dans le cadre du cours INF2005
à l’UQAM
◇ Le(s) nom(s) de(s) développeur (euse)(s)
◇ Chaque nom de développeur (euse) aura un lien permettant de lui
envoyer un courriel
◇ Le mot UQAM aura un lien qui conduit vers une page interne intégrant
la vidéo disponible à l’adresse suivante :
https://www.youtube.com/watch?v=X5DWqN3Xu-0
◇ Le mot INF2005 aura un lien qui s’ouvre sur une nouvelle fenêtre :
http://www.etudier.uqam.ca/cours?sigle=INF2005
— Toutes les pages du site porteront un logo de la CENI. Ce logo sera
associé à un lien qui ramène à la page d’accueil (index.html) du site
— Dans la mesure du possible, on essayera d’ajouter également le pied de
page et le menu de navigation décrits ci-haut dans les autres pages
— Essayer au mieux possible de valider les documents pour se conformer
aux normes HTML 5 et CSS

## 5.2 Consignes et instructions

Cette section contient les informations pratiques pour réaliser et remettre le
TP 1.

### 5.2.1 Plateforme de développement et livrables

Nous suggérons l’utilisation d’un IDE (integrated development environment )
de votre choix. A titre de rappel, nous utilisons Eclipse (http://www.eclipse.
org/) ou Aptana (http://www.aptana.com/) dans ce cours. Toutefois, les étudiants peuvent toujours utiliser leur outil de développement favori. Il est à noter
que pour ce premier TP, seul les images, les fichiers HTML, CSS et
éventuellement Javascript seront acceptés à la livraison. Aucun framework autre que ce qui est autorisé explicitement et aucun autre type de
fichier ne sera accepté. Sauf indication contraire dans l’énoncé, tout ce qui sera
remis devra être le fruit de votre propre travail. Il est strictement interdit
de copier du code sur Internet ou d’intégrer du code généré automatiquement avec un logiciel non autorisé sous peine de se faire accuser
de plagiat.

## 5.2.2 Groupes, collaboration et sauvegarde

Ce travail pratique est à faire en groupe ou équipe de 2 personnes au maximum. La composition de l’équipe devrait rester identique pour le TP 2 aussi.
Les étudiant(e)s qui veulent travailler seul doivent constituer une équipe d’une
personne. Aucune réduction de charge de travail ni aménagement ne sera accordé parce qu’on travaille seul (e). Chaque étudiant devrait se mettre dans un
groupe sur Moodle. Les coéquipiers devraient choisir un même groupe. Avant
de se mettre dans un groupe avec une autre personne, il faut s’assurer de s’être
déjà convenu de travailler ensemble. Le choix de groupe se fait plusieurs semaines avant la remise du TP1 et ne devrait plus changer jusqu’à la fin de
la session. Advenant une préoccupation indépendante de sa volonté, il faudra
contacter l’enseignant pour voir ce qu’il faut faire. L’abandon du cours par un
membre de l’équipe n’est pas une raison de changer de groupe. L’autre membre
éventuel continuera les TP seul.
Pour faciliter le suivi des modifications et limiter les risques de perte de travail en cas de problème avec l’ordinateur, nous suggérons fortement l’utilisation
d’un gestionnaire de code source. L’utilisation d’un git semble bien indiqué tant
pour la sauvegarde que pour la collaboration. Gitlab (http://www.gitlab.com)
et Bitbucket (http://bitbucket.org) offrent la possibilité de créer gratuitement des dépôts public ou privés. Github (https://education.github.com/
students) offre également des solutions gratuites aux étudiants. Il existe aussi
d’autres fournisseurs et on peut héberger son dépôt où on veut. La principale
exigence est que le dépôt git du TP reste privé jusqu’à la fin du cours.

## 5.2.3 Remise

Ce travail est à rendre sur moodle au plus tard le 25 février 2019 à 23H55.
La remise se fera sous forme d’un fichier zip contenant le dossier complet du site
Internet. Seul un membre du groupe fera la remise. L’autre membre éventuel
devra s’assurer que le TP soit bien remis à temps. Il faut se convenir clairement
sur la personne qui va faire cette remise. Le nom du fichier zip à rendre sera
constitué des codes permanents des membres de l’équipe séparés par un trait
d’union. Voici quelques exemples des noms
— groupe de 2 personnes : ABCD123456-EFGD125634.zip
— groupe de 1 personne : ABCD123456.zip
Au besoin, Il est permis d’ajouter dans le dossier du site (fichier zip), un document d’explication